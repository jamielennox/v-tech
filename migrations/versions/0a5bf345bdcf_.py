"""empty message

Revision ID: 0a5bf345bdcf
Revises: 729bd5459944
Create Date: 2017-07-23 16:55:39.431871

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '0a5bf345bdcf'
down_revision = '729bd5459944'
branch_labels = None
depends_on = None


# Putting seed data in a migration is controversial, but for a sample app it
# should be sufficient.


animal_table = sa.sql.table('animal',
    sa.sql.column('name', sa.String),
)


def upgrade():
    op.bulk_insert(animal_table, [
        {'name': 'Lion'},
        {'name': 'Tiger'},
        {'name': 'Bear'}
    ])


def downgrade():
    pass
