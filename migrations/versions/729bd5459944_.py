"""Create a really simple animal table

Revision ID: 729bd5459944
Revises:
Create Date: 2017-07-23 16:41:07.957690

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '729bd5459944'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('animal',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(length=128), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('animal')
    # ### end Alembic commands ###
